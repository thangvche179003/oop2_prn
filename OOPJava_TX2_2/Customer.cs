﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPJava_TX2_2
{
    public class Customer : Person
    {
        public int balance {  get; set; }

        public Customer(string name, string address, int balance) : base(name, address)
        {
            balance = balance;
        }

    

        public override void Display()
        {
            Console.WriteLine();
        }
    }

}
