﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace OOPJava_TX2_2
{
    public class Employee : Person
    {
        public int salary {  get; set; }

        public Employee(string name, string address, int salary) : base(name, address)
        {
            this.salary = salary;
        }

        

        public override void Display()
        {
            Console.WriteLine($"Name: {name}, Address: {address}, Salary: {salary}");
        }
    }

}
