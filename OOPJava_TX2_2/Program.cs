﻿using System.Text;

namespace OOPJava_TX2_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Customer[] customers ={
                new Customer("Nguyen manh hung","Bac ninh",200000),
                new Customer("Hoang ngoc thuy","Vinh Phuc",100000),
                new Customer("Ngo thi thuong","Thach That",100000),
                new Customer("Do duc hanh","Hai phong",50000)

            };
            Employee[] empyloyee =
            {
                new Employee("Le khanh duy","Thanh hóa",300000),
                new Employee("Phan dinh phung","Huong hoa",200000),
                new Employee("Dinh viet chung","Nam dinh",200000),
                new Employee("Vu cong thanh","Hung yen",100000)
            };
            Console.WriteLine("Make a chose:");
            Console.WriteLine("1 Nhân viên có số lương cao nhất và khác hàng có số dư nhỏ nhất");
            Console.WriteLine("2 Tìm nhân viên theo tên:");

            int chose;


            int maxSalary = 0;
            int minBalance = customers[0].balance;

            if (int.TryParse(Console.ReadLine(), out chose))
            {
                if (chose == 1 || chose == 2)
                {
                    switch (chose)
                    {
                        case 1:
                            {

                                for (int i = 1; i < customers.Length; i++)
                                {
                                    if (minBalance > customers[i].balance)
                                    {
                                        minBalance = customers[i].balance;
                                    }
                                }
                                foreach (var sa in empyloyee)
                                {
                                    if (sa.salary > maxSalary)
                                    {
                                        maxSalary = sa.salary;
                                    }
                                }
                                Console.WriteLine($"The customer with the smallest account balance is {minBalance}");
                                Console.WriteLine($"The employee with the highest salary is {maxSalary}");
                                break;
                            }
                        default:
                            {
                                string name;
                                Console.WriteLine("Vui long nhap ten nguoi ban muon tim kiem");
                                name = Console.ReadLine();
                                foreach (var sa in empyloyee)
                                {
                                    if (name.Equals(sa.name))
                                    {
                                        Console.WriteLine(sa.salary);
                                    }

                                }

                                break;

                            }
                    }
                }
                else
                {
                    Console.WriteLine("Không có lựa chọn này");
                }
            }
            else
            {
                Console.WriteLine("Vui lòng nhập đúng định dạng chỉ từ 1 đến 2");
            }
        }
        }
    }

